abstract class BaseConfig {
  String get apiHost;

  String get envName;
}

class StagingConfig implements BaseConfig {
  @override
  String get apiHost => "https://stage.example.com";

  @override
  String get envName => 'Staging';
}

class ProdConfig implements BaseConfig {
  @override
  String get apiHost => "https://prod.example.com";

  @override
  String get envName => 'Production';
}
