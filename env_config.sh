# Replacing the files, based on environment variable value.
cp lib/config/"$1"/google-services.json android/app/google-services.json
cp lib/config/"$1"/GoogleService-Info.plist ios/Runner/GoogleService-Info.plist

if [ "$1" == "production" ]; then

  # iOS files, replacing the bundle id to production.
  sed -i -e 's/com.example.envdemo.staging;/com.example.envdemo.prod;/g' ios/Runner.xcodeproj/project.pbxproj
  sed -i -e 's/\<string\>com.example.envdemo.staging\<\/string\>/\<string\>com.example.envdemo.prod\<\/string\>/g' ios/Runner/Info.plist

  # Android files, replacing the packages id to production.
  sed -i -e 's/"com.example.envdemo.staging"/"com.example.envdemo.prod"/g' android/app/build.gradle

  # main.dart file environment change to prod.
  sed -i -e 's/Environment.STAGING/Environment.PROD/g' lib/main.dart

  # Removing the auto generated files from above command.
  rm ios/Runner.xcodeproj/project.pbxproj-e
  rm android/app/build.gradle-e
  rm ios/Runner/Info.plist-e
  rm lib/main.dart-e

  # Log for successful execution.
  echo "Production setup changes are done."

else

  # iOS files, replacing the bundle it to staging.
  sed -i -e 's/com.example.envdemo.prod;/com.example.envdemo.staging;/g' ios/Runner.xcodeproj/project.pbxproj
  sed -i -e 's/\<string\>com.example.envdemo.prod\<\/string\>/\<string\>com.example.envdemo.staging\<\/string\>/g' ios/Runner/Info.plist

  # Android files, replacing the packages to staging.
  sed -i -e 's/"com.example.envdemo.prod"/"com.example.envdemo.staging"/g' android/app/build.gradle

  # main.dart file environment change to staging.
  sed -i -e 's/Environment.PROD/Environment.STAGING/g' lib/main.dart

  # Android buildTypes signing config change to release.
  sed -i -e 's/signingConfig signingConfigs.release/signingConfig signingConfigs.debug/g' android/app/build.gradle

  # Removing the auto generated files from above command.
  rm ios/Runner.xcodeproj/project.pbxproj-e
  rm android/app/build.gradle-e
  rm ios/Runner/Info.plist-e
  rm lib/main.dart-e

  # Log for successful execution.
  echo "Staging setup changes are done."

fi
